FROM golang:latest

ENV MOUNTSOURCE="encrypted:"
ENV MOUNTDESTINATION="/mnt/drive"
ENV MOUNTCOMMAND="--allow-non-empty --allow-other --log-level INFO --log-file /gclone/logs/rclonecache.log --poll-interval 15s --buffer-size 128M --cache-dir /gclone/cache --vfs-read-chunk-size 1024M --vfs-read-chunk-size-limit off --dir-cache-time 72h --drive-chunk-size 64M"

RUN apt update -y; apt upgrade -y
RUN apt install build-essential fuse -y

WORKDIR /gclone/src
COPY . /gclone/src
RUN go get -v ./...
RUN go mod tidy
RUN go build
RUN mv ./gclone /usr/bin
RUN mkdir -p /gclone/logs/
RUN mkdir -p /gclone/cache/
RUN touch /gclone/logs/rclonecache.log

WORKDIR /gclone/config
CMD /usr/bin/gclone --config /gclone/config/gclone.conf mount ${MOUNTSOURCE} ${MOUNTDESTINATION} ${MOUNTCOMMAND}