  
gclone
====  

<p align="center"><img src="https://gitea.s1.thoxy.xyz/repo-avatars/44-5059d112cf28c77d215fb138e202a7b6" width=170 style="text-align: center;"/>
<br>A modified version of the <a href="https//github.com/rclone/rclone" >rclone</a> created by <a href="https://github.com/donwa/gclone">donwa/gclone</a> updated by Thoxy
</br>Provide dynamic replacement sa file support for google drive operation
</p>

## Installation  

- Just download the latest version on [https://gitea.s1.thoxy.xyz/thoxy/gclone/releases](https://gitea.s1.thoxy.xyz/thoxy/gclone/releases)
- Unzip
- Use

## Instructions 
### 1. service_account_file_path Configuration   
add `service_account_file_path` Configuration.For dynamic replacement service_account_file(sa file). Replace configuration when `rateLimitExceeded` error occurs
`rclone.conf` example:  

```conf
[drive]
type = drive
scope = drive
service_account_file_path = /path/to/accounts/folder
root_folder_id = TD ID or Folder ID
```

`/path/to/accounts/folder` Folder contains multiple access and edit permissions ***service account file(*.json)***.  
  
### 2. Support incoming id
If the original rclone is across team disks or shared folders, multiple configuration drive letters are required for operation.
gclone supports incoming id operation

```console
gclone copy gc:{folde_id1} gc:{folde_id2}  -P --stats=5s --ignore-existing --drive-server-side-across-configs --drive-acknowledge-abuse --drive-keep-revision-forever --tpslimit=6 --tpslimit-burst=8 --transfers=5 --checkers=5 --drive-chunk-size 1024M
```

folde_id1 can be:Common directory, shared directory, team disk. 
  
```console
gclone copy gc:{folde_id1} gc:{folde_id2}/media/  -P --stats=5s --ignore-existing --drive-server-side-across-configs --drive-acknowledge-abuse --drive-keep-revision-forever --tpslimit=6 --tpslimit-burst=8 --transfers=5 --checkers=5 --drive-chunk-size 1024M
```

```console
gclone copy gc:{share_fiel_id} gc:{folde_id2}  -P --stats=5s --ignore-existing --drive-server-side-across-configs --drive-acknowledge-abuse --drive-keep-revision-forever --tpslimit=6 --tpslimit-burst=8 --transfers=5 --checkers=5 --drive-chunk-size 1024M
```

## Mount

### With Service (best method)


First modify the paths to mach yours in the **gclone.service.sample** and copy it to **/etc/systemd/system/gclone.service**

Then put this in your console to reload the systemctl services list

```console
sudo systemctl daemon-reload
```

Then you can use it like all other service.

```console
sudo systemctl start gclone
sudo systemctl enable --now gclone
sudo systemctl stop gclone
sudo systemctl restart gclone
```

### With Docker 

Put your config file in **/home/user/.gclone** (gclone.conf and accounts folder)

docker-compose.yml:

```yml
services:
  gclone:
    devices:
      - "/dev/fuse"
    container_name: gclone
    cap_add:
      - "SYS_ADMIN"
    build: https://gitea.s1.thoxy.xyz/thoxy/gclone.git
    environment:
      - 'MOUNTSOURCE=encrypted:'
      - 'MOUNTDESTINATION=/mnt/drive'
    volumes:
      - /mnt/drive:/mnt/drive:shared
      - /home/user/.gclone/:/gclone/config
```

(I have tested many config and seems to be the most optimized for plex use)

### With gclone (not persistant)
Just use the gclone mount command
```console
/usr/bin/gclone --config /gclone/config/gclone.conf mount MOUNTSOURCE MOUNTDESTINATION MOUNTCOMMAND --allow-non-empty --allow-other --log-level INFO --log-file /gclone/logs/rclonecache.log --poll-interval 15s --buffer-size 128M --cache-dir /gclone/cache --vfs-read-chunk-size 1024M --vfs-read-chunk-size-limit off --dir-cache-time 72h --drive-chunk-size 64M
```

## Warning

It is recommended to encrypt the drive. To make it you can follow this guides :

- https://rclone.org/crypt/
- https://www.maketecheasier.com/use-rclone-crypt-encrypt-files/
- https://blog.thelazyfox.xyz/how-to-mount-an-encrypted-google-drive-folder-with-rclone/


## Credit

- Thanks to [rclone](https://github.com/rclone/rclone) for original repos
- Special thanks to donwa for is rclone (gclone mod) : [https://github.com/donwa/gclone/](https://github.com/donwa/gclone/)